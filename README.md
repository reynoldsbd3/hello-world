# hello-world

A very simple example of using sel4-sys and sel4-start to create an initial
thread executable. Here's how you can test it out...

## Set up a build environment

If you haven't yet, [set up a build
environment](https://robigalia.org/build-environment.html).

Also install qemu or another hypervisor if you want to test in a VM.

## Build seL4

Clone https://gitlab.com/robigalia/sel4, initialize submodules, and run `make
x86_defconfig && make`. This will put a kernel into
`sel4/build/kernel/kernel.elf`.

## Build this crate

    cargo build --target i686-sel4-unknown

## Run it in a VM

    qemu-system-i386 -nographic -kernel path/to/sel4/build/kernel/kernel.elf.strip -initrd path/to/hello-world/target/i686-sel4-unknown/debug/hello-world -cpu Haswell

You'll see some hardware-related debug spam and then

	Starting node #0
	Hello, world!
	Caught cap fault in send phase at address 0x0
	while trying to handle:
	user exception 0x6 code 0x0
	in thread 0xe01f9900 "rootserver" at address 0x804865d

Yay! If you forget `-nographic`, qemu will display the VGA buffer. This
example prints to the serial port, though, so you won't see anything.

If you forget `-cpu Haswell`, sel4 will complain about `XSAVE` not being
supported.

To quit out of the qemu console, use the command `Ctrl-a x` (that is, hold
control and press a, release both, then press x).
